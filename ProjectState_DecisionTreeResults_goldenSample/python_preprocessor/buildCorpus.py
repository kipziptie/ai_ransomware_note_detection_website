from myGlobals import *
import string, re
try:
    import nltk
except ImportError:
    print("attempting to install NLTK python module for you")
    os.system('python -m pip install NLTK')
    import nltk
nltk.download('stopwords')

stopwords = nltk.corpus.stopwords.words('english')
print(stopwords)
punctuation = string.punctuation
################################
# stripper() - A bad character stripping function
#
# Purpose: to remove characters which should not be seen by the machine learning model.
################################
def stripper(items, badlist, deep=True):
    if deep == False:
        stripped = [word.lower() for word in items if not word.lower() in badlist]
    else:
        stripped = [re.sub(r'[^\w\s]', '', word.lower()) for word in items if not word.lower() in badlist]
    return(stripped)

def removeNumbers(items):
    stripped = [word for word in items if word is not word.isnumeric() ]
    return(stripped)


def buildCorpus(files, removestopwords=False, removepunctuation=False, negativeSamples=False):
    CORPUS = []
    for f in files:
        content = ''
        if negativeSamples == False:
            filepath = os.path.join(CLEAN_NOTE_DIR, f)
        elif negativeSamples == True:
            filepath = os.path.join(CLEAN_NEGATIVE_SAMPLES_DIR, f)
        print(filepath)

        with open(filepath, "r", encoding="utf8") as file:
            try:
                # Get the raw file content in text format
                content = file.read()

            except UnicodeDecodeError as e:
                print("EXCEPTION: Failed to read ", filepath)
                print(e)
                # Add file reference to list of skipped files to try later
                SKIPPED.append(filepath)

            try:
                # tokens = nltk.tokenize.word_tokenize(cleanedContent)
                tokens = content.split(" ")
            except LookupError:
                print("NLTK Dependency Missing. Downloading PUNKT")
                nltk.download('punkt')
                try:
                    tokens = nltk.tokenize.word_tokenize(content)
                except:
                    print("Unknown Error")
                    exit(-1)
            print(tokens)
            ################################
            # REMOVE STOPWORDS and PUNCTUATION
            ################################


            if removestopwords == True:
                tokens = stripper(tokens, stopwords)
            # print("Tokenized Words: ", tokens)

            if removepunctuation == True:
                tokens = stripper(tokens, punctuation)

            tokens = removeNumbers(tokens)

            print("Stripped Tokenized Words: ", tokens)

            # writeOutfile(tokens)

            # Build a Corpus
            # The corpus is the collection of all ransomware samples contained in the set.
            CORPUS.append(tokens)

    return CORPUS