import html2text, os
# manual:  https://pypi.org/project/html2text/
FILE_DIR = os.path.dirname("./htmlFiles/")
files = os.listdir(FILE_DIR)

def stripEmptyListItems(input):
    out = [x for x in input if x]
    return(out)

def writeOut(input):
    path = os.path.relpath("outfile.txt")
    with open(path, "a") as file:
        for item in input:
            x = item.replace("\n", "")
            x = x.replace("** ] ", "** ] \n")
            x = x.replace("[ **", "\n\n[ ** ")
            file.write(x)

OUT_CONTENT = []

for f in files:
    filepath = os.path.join(FILE_DIR, f)
    print(filepath)
    with open(filepath, "r") as file:
        content = file.readlines()
        for line in content:
            text = html2text.html2text(line)
            print(text.strip())
            OUT_CONTENT.append(text)
    writeOut(OUT_CONTENT)
print(type(text))
