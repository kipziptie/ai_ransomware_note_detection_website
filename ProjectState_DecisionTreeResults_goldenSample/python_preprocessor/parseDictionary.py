import os
DICTIONARY = "dictionary.corpora"
filepath = os.path.relpath(DICTIONARY)
SKIPPED = []
with open(filepath, "r", encoding="utf8") as file:
    try:
        # Get the raw file content in text format
        content = file.read()

        #print("Raw Content:\n", content)
    except UnicodeDecodeError as e:
        print("EXCEPTION: Failed to read ", filepath)
        print(e)
        # Add file reference to list of skipped files to try later
        SKIPPED.append(filepath)

lines = content.split('\n')

# the first line in a corpora dictionary is a single integer representing the number of columns.
columns = lines[0]
lines.pop(0)

# remove any lines which are empty lists
lines.remove('')

# Each line is parsed into 3 columns (INDEX, CONTENT, COUNT)
for line in lines:
    print(line.split('\t'))
    token = line.split('\t')[1]
    print(token)

