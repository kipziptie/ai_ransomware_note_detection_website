import os
from curses.ascii import isprint

def printable(input):
    return ''.join(char for char in input if isprint(char))

def writeOutfile(content):
    path=os.path.relpath("outfile.txt")
    with open(path, "a") as file:
        file.write(str(content))
        file.write("\n")
def cleanFiles(infile):
    #for f in files:
        content=''
        filepath = os.path.relpath(infile)
        print(filepath)

        with open(filepath, "r", encoding="utf8") as file:
            try:
                # Get the raw file content in text format
                content = file.read()

                # strip all characters which are not copnsidered "printable" e.g. NULL /x00. Also Mark all URLs with
                # the special ##URL## constant
                cleanedContent = printable(content)

                print("Raw Content:\n", content)
                print("Cleaned Content:\n", cleanedContent)
            except UnicodeDecodeError as e:
                print("EXCEPTION: Failed to read ", filepath)
                print(e)
                # Add file reference to list of skipped files to try later
                print("SKIPPED", filepath)
            writeOutfile(cleanedContent)

infile = "toClean.log"
#cleanFiles(infile)

infile = "infile.txt"

import re
with open(infile) as f:
    for line in f:
        print(line)

        parsed = re.findall(r'"([^"]*)"', line)
        print(parsed)
        writeOutfile(parsed)

        parsed = str.split(str(parsed),'"')
        print(parsed)