# How-To Try-Catch: https://www.codingem.com/try-catch-in-python/
#
import os, string, re, html2text
from myGlobals import *
from textProcessing import *
# Import for cleaning non-printable characters

deleteDirectory(CLEAN_NOTE_DIR)
deleteDirectory(CLEAN_NEGATIVE_SAMPLES_DIR)

def html2md(html):
    parser = HTML2Text()
    parser.ignore_images = True
    parser.ignore_anchors = True
    parser.body_width = 0
    md = parser.handle(html)
    return md

def htmlToText(input):
    # since i want to adjust some options, I must define a parser
    parser = html2text.HTML2Text()
    parser.ignore_images = True
    parser.ignore_anchors = True

    # body_width = 0 prevents text wrapping. This is important to prevent, otherwise two words get joined together
    # during a linebreak.
    parser.body_width = 0
    text = parser.handle(input)
    #text = html2text.html2text(input)
    return(text)

files=getDirContents(RAW_NOTE_DIR)
textFiles = getFilesByExtension(files, ".txt")
htmlFiles = getFilesByExtension(files, ".html")
print("HTML FILES: ", htmlFiles)
# List contents of raw notes, TXT notes only
#print(textFiles)

###############################################################
# Build a directory of ransomware note samples which are uniform
# and machine readable.
###############################################################
filesToProcess = textFiles + htmlFiles
cleanFiles(filesToProcess, False)


###############################################################
# Build a directory of non-ransomware note samples which are uniform
# and machine readable.
###############################################################
files=getDirContents(NEGATIVE_NOTE_DIR)
filesToProcess = getFilesByExtension(files, ".txt")
cleanFiles(filesToProcess, True)

