## The feature selection should be the allowed words
# Stopword removal happens here
# dictionary cleaning
# replace e-mail addresses with named variables
# replace bitcoin addresses with named variables
# identify random character strings (which could be wallet addresses or other crazy shi-... stuff... )

import os, nltk, re, pickle
from gensim import corpora
from buildCorpus import *
import matplotlib.pyplot as plt
import pandas as pd
import numpy as np


SKIPPED = []
DEBUG = True

CLEAN_NOTE_DIR = os.path.dirname("./cleanedNotes/")
CLEAN_NEGATIVE_SAMPLES_DIR = os.path.dirname("./cleanedNegativeSamples/")
SHOWPLOTS = True
TUNED_CONSTANTS = False

def getDirContents(dir):
    return os.listdir(dir)

########################################################
# buildCorpus() - Builds a Bag-of-Words Corpus from a list of text files
#
# Purpose - Transforms a directory of files into a Corpus dictionary for future machine learning model ingestion.
########################################################


def LSA_MODEL():
    # Can we transform this bag of words into a different model?
    from gensim import models

    lsa = models.LsiModel(bow_corpus, id2word=dictionary, num_topics=3)
    print(lsa)

    test_doc=[(1,1),(3,1)]
    print(lsa[test_doc])

    corpus_lsa=lsa[bow_corpus]
    i=0
    for x in corpus_lsa:
        print(i,":",x)
        i+=1

def outputBoW(bow, name):
    outfile = np.array(bow)
    dir = os.path.join("bow", name)
    np.save(dir, outfile, allow_pickle=True)

def outputCorpus(corpus, name):
    outfile = corpus
    dir = os.path.join("corpus", name)

    with open(dir, "wb") as f:
        pickle.dump(corpus, f)


def barplot_counts(bag_of_words, amount, title=""):
    #Convert dictionary array to a pandas dataframe
    dataframe = pd.DataFrame(bag_of_words.most_common(amount))
    #Sets index to column '0', which holds the string value for each entry,
    #col '1' contains the count
    dataframe.set_index(0,drop=True,inplace=True)
    #Define the bar plot
    dataframe.plot(kind="bar", legend = False)
    plt.xlabel("Words")
    plt.ylabel("Count")
    plt.suptitle(title)
    plt.subplots_adjust(top = .9, bottom = 0.25)

# LSA_MODEL()

textFiles=getDirContents(CLEAN_NOTE_DIR)
corpus1 = buildCorpus(textFiles)
corpus2 = buildCorpus(textFiles, True, False)
corpus3 = buildCorpus(textFiles, True, True)

print(getDirContents(CLEAN_NEGATIVE_SAMPLES_DIR))
textFiles=getDirContents(CLEAN_NEGATIVE_SAMPLES_DIR)
print(textFiles)
corpusNegative = buildCorpus(textFiles, True, True, True)
print(corpusNegative)

outputCorpus(corpus1, "corpus1")
outputCorpus(corpus2, "corpus2")
outputCorpus(corpus3, "corpus3")
outputCorpus(corpusNegative, "corpusNegative")

#pprint.pprint(CORPUS)

# Now convert the Corpus into a list of Vectors
dictionary1 = corpora.Dictionary(corpus1)
dictionary2 = corpora.Dictionary(corpus2)
dictionary3 = corpora.Dictionary(corpus3)
dictionaryNegative = corpora.Dictionary(corpusNegative)

if DEBUG == True:
    print("THE DICTIONARY\n", dictionary3)

# Save the dictionary to a file.
#outfile = open("dictionary.corpora", "wb")
#dictionary.save_as_text(outfile, sort_by_word=True)
#outfile.close()


#pprint.pprint(dictionary.token2id)

bow_corpus1 = [dictionary1.doc2bow(text) for text in corpus1]
bow_corpus2 = [dictionary2.doc2bow(text) for text in corpus2]
bow_corpus3 = [dictionary3.doc2bow(text) for text in corpus3]
bow_corpusNegative = [dictionaryNegative.doc2bow(text) for text in corpusNegative]


#print("Here, we have the Bag of Words BoW vector representation of my CORPUS")
#pprint.pprint(bow_corpus)





##########################
# Print the common words #
##########################
#high_frequency_words = dictionary.most_common(20)
#for word in high_frequency_words:
#    print(word[0])


barplot_counts(dictionary1, 20, "Top 20 most frequent words, Corpus1")
barplot_counts(dictionary2, 20, "Top 20 most frequent words, Corpus2")
barplot_counts(dictionary3, 20, "Top 20 most frequent words, Corpus3")
barplot_counts(dictionaryNegative, 20, "Top 20 most frequent words from the negative set, in english")
if SHOWPLOTS == True:
    plt.show()
# Save the created CORPUS as a numpy array

outputBoW(bow_corpus1, "bow1")
outputBoW(bow_corpus2, "bow2")
outputBoW(bow_corpus3, "bow3")
outputBoW(bow_corpusNegative, "bowNegative")
