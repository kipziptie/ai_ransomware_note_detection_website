// file writing example: https://nodejs.dev/es/learn/writing-files-with-nodejs/

const express = require('express')
const app = express()
var bodyparser = require("body-parser")
const Fs = require('fs');

var urlencodedparser = bodyparser.urlencoded({extended: false})

app.get('/',  (req, res) => {
    res.sendFile('index.html', { root: __dirname })
  }
)

app.post('/predict', urlencodedparser, (req, res) => {
    const status = 400
    
    try {
    	Fs.writeFileSync('./samples/file.txt', req.body.note);
    } catch (Err) {
    	console.error(Err);
    }
    
   
    
    //res.status(status).send(req.body)
    res.redirect('/predict')
    
    
})

app.get('/predict', (req, res) => {

    const { spawn } = require('child_process');
    const pyProg = spawn('python3', ['./predictFile.py']);

    pyProg.stdout.on('data', function(data) {

        console.log(data.toString());
        res.write(data);
        res.end();
    });
})

app.listen(80, () => console.log('Application listening on port 80!'))
